using System;
using System.Text;
using System.Collections;

namespace com.datamonitor.tokenWebService
{
    /// <summary>
    /// Object representing a token.
    /// </summary>
    public class Token
    {
        private string _token;
        private DateTime _createDate;
        private DictionaryEntry[] _metadata;

        /// <summary>
        /// Generates and returns a random string of given size.
        /// </summary>
        /// <param name="size">The length of string to return.</param>
        /// <returns>String of random characters.</returns>
        private string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            return builder.ToString();
        }

        /// <summary>
        /// Constructs a token for the specified metadata.
        /// </summary>
        /// <param name="metadata">Metadata to be maintained within the token.</param>
        public Token(DictionaryEntry[] metadata)
        {
            _token = RandomString(64);
            _createDate = DateTime.UtcNow;
            _metadata = metadata;
        }

        /// <summary>
        /// Constructs a token for an existing information.
        /// </summary>
        /// <param name="token">The token for which this object is based.</param>
        /// <param name="createDate">The creation date for the token originally.</param>
        /// <param name="metadata">The metadata held with the token.</param>
        public Token(string token, DateTime createDate, string metadata)
        {
            _token = token;
            _createDate = createDate;
            _metadata = new DictionaryEntry[0];

            // Deserialise the metadata back into the array
            if (!string.IsNullOrEmpty(metadata) && metadata.Length > 0)
            {
                System.Xml.Serialization.XmlSerializer xmlMetadataSerializer = new System.Xml.Serialization.XmlSerializer(_metadata.GetType());
                _metadata = xmlMetadataSerializer.Deserialize(new System.IO.StringReader(metadata)) as DictionaryEntry[];
            }

        }

        /// <summary>
        /// Returns the string of the token.
        /// </summary>
        /// <returns>String of the token.</returns>
        public override string ToString()
        {
            return _token;
        }

        /// <summary>
        /// Creation date of the token.
        /// </summary>
        public DateTime CreateDate
        {
            get { return _createDate; }
        }

        /// <summary>
        /// Metadata held with the token.
        /// </summary>
        public DictionaryEntry[] MetaData
        {
            get { return _metadata; }
        }

        /// <summary>
        /// Serialise the metadata held with the token.
        /// </summary>
        /// <returns>String Xml of metadata.</returns>
        public string MetaDataXml()
        {
            using (System.IO.StringWriter sw = new System.IO.StringWriter())
            {
                System.Xml.XmlTextWriter xw = new System.Xml.XmlTextWriter(sw);

                if (_metadata != null)
                {
                    System.Xml.Serialization.XmlSerializer xmlMetadataSerializer = new System.Xml.Serialization.XmlSerializer(_metadata.GetType());
                    xmlMetadataSerializer.Serialize(xw, _metadata);
                }

                xw = null;

                return sw.ToString();
            }
        }

        /// <summary>
        /// Checks if the token is valid based of configured validation.
        /// </summary>
        /// <returns>Boolean representing if the token is valid.</returns>
        public bool IsValid()
        {
            return ((DateTime.UtcNow - _createDate).TotalSeconds < Properties.Settings.Default.TokenValiditySeconds);
        }
    }
}
