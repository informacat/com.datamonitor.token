using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Collections;
using System.Collections.Generic;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.ComponentModel;

namespace com.datamonitor.tokenWebService
{
    /// <summary>
    /// TokenWebService for generating time limited tokens which hold metadata to be communicated between applications.
    /// </summary>
    [WebService(Namespace = "http://api.datamonitor.com/token", Name = "TokenWebService V1", Description = "TokenWebService for generating time limited tokens which hold metadata to be communicated between applications.")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [ToolboxItem(false)]
    public class v1 : System.Web.Services.WebService
    {
        /// <summary>
        /// Creates a time limited token to communicate metadata between 2 applications / sites.
        /// </summary>
        /// <param name="Metadata">Array of DictionaryEntry objects (Key / Value pairs).</param>
        /// <returns>Token string to retrieve information.</returns>
        [WebMethod(Description = "Creates a time limited token to communicate metadata between 2 applications / sites.")]
        public string CreateToken(DictionaryEntry[] Metadata)
        {
            // Generate a new token containing metadata
            Token token = new Token(Metadata);

            // Execute SP to store token and metadata
            using (SqlConnection sqlConn = new SqlConnection(Properties.Settings.Default.TokenDB))
            {
                using (SqlCommand sqlComm = new SqlCommand("dbo.InsertToken", sqlConn))
                {
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["SQLCommandTimeout"] ?? "30");
                    sqlComm.Parameters.AddRange(
                        new SqlParameter[3] {
                            new SqlParameter("@token", token.ToString()),
                            new SqlParameter("@createDate", DateTime.UtcNow),
                            new SqlParameter("@metadata", token.MetaDataXml())
                        });

                    sqlConn.Open();
                    int returnValue = sqlComm.ExecuteNonQuery();
                    sqlConn.Close();
                
                    // Return the token
                    return token.ToString();
                }
            }
        }

        /// <summary>
        /// Validates the specified token and returns the corresponding metadata. The token is a single use token so can't be retrieved twice.
        /// </summary>
        /// <param name="Token">String token to be validated.</param>
        /// <returns>Array of DictionaryEntry objects if the token is valid and within it's time interval.</returns>
        [WebMethod(Description = "Validates the specified token and returns the corresponding metadata. The token is a single use token so can't be retrieved twice.")]
        public DictionaryEntry[] ValidateToken(string Token)
        {
            // Retrieve the tokens information from the DB
            using (SqlConnection sqlConn = new SqlConnection(Properties.Settings.Default.TokenDB))
            {
                using (SqlCommand sqlComm = new SqlCommand("dbo.GetToken", sqlConn))
                {
                    sqlComm.CommandType = CommandType.StoredProcedure;
                    sqlComm.CommandTimeout = int.Parse(ConfigurationManager.AppSettings["SQLCommandTimeout"] ?? "30");
                    sqlComm.Parameters.Add(new SqlParameter("@token", Token));

                    sqlConn.Open();
                    using (SqlDataReader sqlReader = sqlComm.ExecuteReader())
                    {
                        // Check token returned results
                        if (sqlReader.Read())
                        {
                            // Create token with information held within the DB
                            Token token = new Token(
                                sqlReader.GetString(0),
                                sqlReader.GetDateTime(1),
                                sqlReader.GetString(2)
                            );

                            // Check token is valid and return metadata
                            if (token.IsValid())
                                return token.MetaData;
                        }
                    }
                    sqlConn.Close();
                }
            }

            // Token validation failure...
            return null;
        }
    }
}
