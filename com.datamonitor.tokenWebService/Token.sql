

USE Token
GO

EXEC sp_grantdbaccess 'TokenWebService'
GO


CREATE TABLE dbo.Token (
	token				CHAR(64)				NOT NULL,
	createDate			DATETIME				NOT NULL,
	metadata			XML						NULL,
	CONSTRAINT PK_Token PRIMARY KEY (token)
)
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.InsertToken') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.InsertToken
GO

CREATE PROCEDURE dbo.InsertToken (
	@token				CHAR(64),
	@createDate			DATETIME,
	@metadata			XML
)
AS
	INSERT INTO dbo.Token (token, createDate, metadata)
	VALUES (@token, @createDate, @metadata)
GO

GRANT EXEC ON dbo.InsertToken TO TokenWebService
GO


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'dbo.GetToken') AND type in (N'P', N'PC'))
DROP PROCEDURE dbo.GetToken
GO

CREATE PROCEDURE dbo.GetToken (
	@token				CHAR(64)
)
AS
	SELECT *
	FROM dbo.Token
	WHERE token = @token
	
	DELETE FROM dbo.Token
	WHERE token = @token
GO

GRANT EXEC ON dbo.GetToken TO TokenWebService
GO
